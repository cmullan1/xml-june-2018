<?xml version="1.0" encoding="UTF-8"?>
<!-- XML Assignment #3       -->
<!-- Author:  Corinne Mullan -->
<!-- Date:    June 22, 2018  -->
<!-- Nice work here Corinne. Your xsl properly formats and outputs
the xml data, and you have comments, so well done
10/10
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
		<html>
			<head>
				<title>Phone Bill</title>
			</head>
			<body>
			
				<!-- Output the customer information -->
				<h3>Customer Info</h3>
				<p>Name: <xsl:value-of select="telephoneBill/customer/name"/>
					<br/>
				Address: <xsl:value-of select="telephoneBill/customer/address"/>
					<br/>
				City: <xsl:value-of select="telephoneBill/customer/city"/>
					<br/>
				Province: <xsl:value-of select="telephoneBill/customer/province"/>
					<br/>
				</p>
				
				<!-- Generate a table showing all calls made by the customer -->
				<table border="1">
					<tbody>
						<tr>
							<th>Called Number</th>
							<th>Date</th>
							<th>Duration in Minutes</th>
							<th>Charge</th>
						</tr>
						<xsl:for-each select="telephoneBill/call">
							<tr>
								<td>
									<xsl:value-of select="@number"/>
								</td>
								<td>
									<xsl:value-of select="@date"/>
								</td>
								<td>
									<xsl:value-of select="@durationInMinutes"/>
								</td>
								<td>
									<xsl:value-of select="@charge"/>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
